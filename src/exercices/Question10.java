package exercices;

import donnees.Personne;
import static utilitaires.UtilDate.ageEnAnnees;
import utilitaires.UtilDojo;
import static utilitaires.UtilDojo.categories;


public class Question10 {

  public static void main(String[] args) {
     
      int nbCateg=UtilDojo.categories.length;
    
      int[][] effectif= new int[2][nbCateg];
      
//<editor-fold  desc="ICI VOUS DEVEZ ECRIRE LE CODE QUI PERMET DE REMPLIR LE TABLEAU effectif A PARTIR DE LA LISTE ClubJudo.listeDesMembres">
    String cat=" ";
            for(int i=0; i < UtilDojo.categories.length; i++){
                cat=categories[i];
                for ( Personne p : donnees.Club.listeDesPersonnes){ 

                    if(utilitaires.UtilDojo.determineCategorie(p.sexe, p.poids).equals(cat)){
                        if ( p.sexe.equals("M") ) {
                            effectif[1][i]+=1;
                        }
                        else{
                            effectif[0][i]+=1;
                        }
                    }
            }
            
        }    
//</editor-fold>
    
      System.out.println("\n Répartition des effectifs par sexes et catégories de poids\n");
      System.out.println("   slg mlg  lg mmy  my mld  ld ");
      System.out.println();
      for(int iSexe=0;iSexe<2;iSexe++){
     
          if(iSexe==0)System.out.print(" f "); else System.out.print(" h ");
          
          for(int iCateg=0;iCateg<nbCateg;iCateg++ ){
          
              System.out.printf("%3d ",effectif[iSexe][iCateg]);
          }
          System.out.println(); 
      }
  }
}





