
package exercices;

import donnees.Personne;

public class Question05 {

    public static void main(String[] args) {
      float compteur=0;
      float som=0;
      float moy=1;
      
      for ( Personne p : donnees.Club.listeDesPersonnes){
              
           if ( p.sexe.equals("F") ) {
              som+=p.poids;
              compteur+=1;
           } 
        }  
        moy=som/compteur;  
        System.out.printf("Moyenne des poids des judokas féminins : %5.2f kg\n",moy);  
    }
}

