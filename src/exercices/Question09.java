package exercices;

import donnees.Personne;
import static utilitaires.UtilDate.ageEnAnnees;

public class Question09 {

  public static void main(String[] args) {
    float compteur=0;
    float som=0;
    float moy;
      
    for ( Personne p : donnees.Club.listeDesPersonnes){
            if(utilitaires.UtilDojo.determineCategorie(p.sexe, p.poids).equals("légers")) {
                som=som+ageEnAnnees(p.dateNaiss);
                compteur=compteur+1;
            } 
    }  
    
    moy=som/compteur;
    // Sur votre PDF vous avez 22.5, moi j'ai 24.5 et vu que vous avez pas précisé j'ai fais pour les deux sexes de la catégorie légers
    System.out.printf("Moyenne d'âge des membres du club de catégorie légers : %5.1f ans",moy);  

   }
}



