
package exercices;

import static donnees.Club.listeDesPersonnes;
import donnees.Personne;


public class Question04 {

    public static void main(String[] args) {
       int homme=0, femme = 0;
       int total;
       System.out.println("Effectif du club");  
        for ( Personne p : donnees.Club.listeDesPersonnes){
              
           if ( p.sexe.equals("F") ) {
               femme+=1;
           } 
           if ( p.sexe.equals("M") ) {
               homme+=1;
           } 
            
        }  
          
        System.out.printf("Effectif masculin : %2d\n",homme); 
        System.out.printf("Effectif féminin : %3d\n",femme); 
        total=homme+femme;
        System.out.printf("Effectif total : %5d\n",total);
    }
}

