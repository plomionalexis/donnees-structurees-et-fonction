
package exercices;

import donnees.Personne;

public class Question06 {

    public static void main(String[] args) {
      float max=1000;
      
      for ( Personne p : donnees.Club.listeDesPersonnes){
              
           if ( p.sexe.equals("M") ) {
              if(p.poids<max){
                  max=p.poids;
              }
           } 
        }  
        System.out.printf("Poids le faible parmi les judokas hommes : %5.0f kg\n",max);  
    }
}

