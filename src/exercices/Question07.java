
package exercices;

import donnees.Personne;

public class Question07 {

    public static void main(String[] args) {
          
      float max=1000;
      String nom="";
      String prenom="";
      for ( Personne p : donnees.Club.listeDesPersonnes){
              
           if ( p.sexe.equals("M") ) {
              if(p.poids<max){
                  max=p.poids;
                  nom=p.nom;
                  prenom=p.prenom;
              }
           } 
        }  
        System.out.printf("Judoka masculin le plus léger: %-5s %-5s %5.0f kg\n",prenom,nom,max);  
      
    }
}



