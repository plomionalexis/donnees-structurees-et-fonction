
package exercices;

import static donnees.Club.listeDesPersonnes;
import donnees.Personne;
import static utilitaires.UtilTriListe.trierLesPersonnesParNomPrenom;

public class Question02 {

    public static void main(String[] args) {
        System.out.println("Liste des membres du Club de sexe féminins");  
         trierLesPersonnesParNomPrenom(listeDesPersonnes);       
        for ( Personne p : donnees.Club.listeDesPersonnes){
              
           if ( p.sexe.equals("F") ) {
           
              System.out.printf("%-15s %-20s\n",
                                p.nom,
                                p.prenom
                               );
           
           } 
            
        }  
          
        System.out.println("\n\n");  
    }      
              
    }

    


