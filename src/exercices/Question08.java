package exercices;

import donnees.Personne;
import donnees.Club;
import utilitaires.UtilDate;
import static utilitaires.UtilDate.ageEnAnnees;
import static utilitaires.UtilDate.ageEnAnnees;
import utilitaires.UtilDojo;

public class Question08 {

  public static void main(String[] args) {
     System.out.println("Liste des membres masculins de la catégorie légers âgés de 20 ans ou plus au : "+utilitaires.UtilDate.aujourdhuiChaine());
     for ( Personne p : donnees.Club.listeDesPersonnes){
           if( p.sexe.equals("M") && ageEnAnnees(p.dateNaiss) >= 20){
            if (utilitaires.UtilDojo.determineCategorie(p.sexe, p.poids).equals("légers")) {
                    System.out.println(p.prenom+" "+p.nom+" "+utilitaires.UtilDate.dateVersChaine(p.dateNaiss)+" "+p.poids+"kg "+p.ville);
                
            } 
           }
        }  
   
   }
}



