package donnees;

import java.util.LinkedList;
import java.util.List;
import static utilitaires.UtilDate.*;

public class Club {
      
  public static List<Personne> listeDesPersonnes= new LinkedList();  
     
  //<editor-fold defaultstate="collapsed" desc="CODE REMPLISSANT LA LISTE ListeDesPersonnes ">
  static {
      
      
      // Création d'une  "fiche"  de type Personne et affectation à la variable p1
      
      Personne p1= new Personne();
      
      // Affectation des divers champs de la fiche p1 à une valeur
      
      p1.nom="Durant";
      p1.prenom="Pierre";
      p1.sexe="M";
      p1.poids=83;
      p1.dateNaiss=chaineVersDate("12/05/1993");
      p1.ville="Arras";
      p1.nbVictoires=9;
      
      //<editor-fold defaultstate="collapsed" desc="Création des 11 autres fiches">
      
      Personne p2= new Personne();
      p2.nom="Martin";
      p2.prenom="Sophie";
      p2.sexe="F";
      p2.poids=52;
      p2.dateNaiss=chaineVersDate("25/11/1991");
      p2.ville="Lens";
      p2.nbVictoires=6;
      
      Personne p3= new Personne();
      p3.nom="Lecoutre";
      p3.prenom="Thierry";
      p3.sexe="M";
      p3.poids=72;
      p3.dateNaiss=chaineVersDate("05/08/1992");
      p3.ville="Arras";
      p3.nbVictoires=5;
      
      Personne p4= new Personne();
      p4.nom="Duchemin";
      p4.prenom="Fabienne";
      p4.sexe="F";
      p4.poids=61;
      p4.dateNaiss=chaineVersDate("14/3/1992");
      p4.ville="Lens";
      p4.nbVictoires=10;
      
      Personne p5= new Personne();
      p5.nom="Duchateau";
      p5.prenom="Jacques";
      p5.sexe="M";
      p5.poids=91;
      p5.dateNaiss=chaineVersDate("18/07/1992");
      p5.ville="Bapaume";
      p5.nbVictoires=4;
      
      Personne p6= new Personne();
      p6.nom="Lemortier";
      p6.prenom="Laurent";
      p6.sexe="M";
      p6.poids=76;
      p6.dateNaiss=chaineVersDate("18/02/1989");
      p6.ville="Arras";
      p6.nbVictoires=12;
      
      Personne p7= new Personne();
      p7.nom="Dessailles";
      p7.prenom="Sabine";
      p7.sexe="F";
      p7.poids=68;
      p7.dateNaiss=chaineVersDate("23/03/1990");
      p7.ville="Arras";
      p7.nbVictoires=3;
      
      Personne p8= new Personne();
      p8.nom="Bataille";
      p8.prenom="Boris";
      p8.sexe="M";
      p8.poids=102;
      p8.dateNaiss=chaineVersDate("17/11/1991");
      p8.ville="Vitry-En-Artois";
      p8.nbVictoires=7;
      
      Personne p9= new Personne();
      p9.nom="Lerouge";
      p9.prenom="Laëtitia";
      p9.sexe="F";
      p9.poids=46;
      p9.dateNaiss=chaineVersDate("09/10/1992");
      p9.ville="Lens";
      p9.nbVictoires=4;
      
      
      Personne p10= new Personne();
      p10.nom="Renard";
      p10.prenom="Paul";
      p10.sexe="M";
      p10.poids=68;
      p10.dateNaiss=chaineVersDate("16/08/1992");
      p10.ville="Lens";
      p10.nbVictoires=9;
      
      Personne p11= new Personne();
      p11.nom="Durant";
      p11.prenom="Jacques";
      p11.sexe="M";
      p11.poids=75;
      p11.dateNaiss=chaineVersDate("13/04/1990");
      p11.ville="Arras";
      p11.nbVictoires=4;
      
      Personne p12= new Personne();
      p12.nom="Delespaul";
      p12.prenom="Martine";
      p12.sexe="F";
      p12.poids=55;
      p12.dateNaiss=chaineVersDate("25/02/1991");
      p12.ville="Lens";
      p12.nbVictoires=6;
      //</editor-fold>
      
      
      // Ajout des 12 fiches à la liste  listeDesPersonnes
      
      listeDesPersonnes.add(p1);  listeDesPersonnes.add(p2);
      listeDesPersonnes.add(p3);  listeDesPersonnes.add(p4);
      listeDesPersonnes.add(p5);  listeDesPersonnes.add(p6);
      listeDesPersonnes.add(p7);  listeDesPersonnes.add(p8);
      listeDesPersonnes.add(p9);  listeDesPersonnes.add(p10);
      listeDesPersonnes.add(p11); listeDesPersonnes.add(p12);
      
  }
  //</editor-fold>
}
