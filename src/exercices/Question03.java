
package exercices;

import static donnees.Club.listeDesPersonnes;
import donnees.Personne;
import static utilitaires.UtilTriListe.trierLesPersonnesParNomPrenom;

public class Question03 {

    public static void main(String[] args) {
         System.out.println("Liste des membres masculins du Club pesant plus de 80 kg");  
         trierLesPersonnesParNomPrenom(listeDesPersonnes);       
        for ( Personne p : donnees.Club.listeDesPersonnes){
              
           if ( p.sexe.equals("M") && p.poids >= 80) {
           
              System.out.printf("%-15s %-15s %-5skg %-50s\n",
                                p.nom,
                                p.prenom,
                                p.poids,
                                p.ville
                               );
           
           } 
            
        } 
          
        System.out.println("\n\n");  
    }      
      
    }


